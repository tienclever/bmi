package com.android.base.navigation

import android.os.Bundle

interface AppNavigation : BaseNavigation {
    fun openSplashToTop(bundle: Bundle? = null)

    fun openTopToDetail(bundle: Bundle? = null)
}
package com.android.base.navigation

import android.os.Bundle
import com.android.base.R
import javax.inject.Inject

class AppNavigationImpl @Inject constructor() : BaseNavigationImpl(), AppNavigation {

    override fun openSplashToTop(bundle: Bundle?) {
        openScreen(R.id.action_splashFragment_to_topFragment, bundle)
    }

    override fun openTopToDetail(bundle: Bundle?) {
        openScreen(R.id.action_topFragment_to_detailFragment, bundle)
    }
}
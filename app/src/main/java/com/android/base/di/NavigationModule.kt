package com.android.base.di

import com.android.base.navigation.AppNavigation
import com.android.base.navigation.AppNavigationImpl
import com.android.base.navigation.BaseNavigation
import com.android.base.navigation.BaseNavigationImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped

@Module
@InstallIn(ActivityComponent::class)
abstract class NavigationModule {
    @Binds
    @ActivityScoped
    abstract fun provideBaseNavigation(navigation: BaseNavigationImpl): BaseNavigation

    @Binds
    @ActivityScoped
    abstract fun provideAppNavigation(navigation: AppNavigationImpl): AppNavigation
}
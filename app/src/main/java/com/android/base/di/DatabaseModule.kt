package com.android.base.di

import android.content.Context
import androidx.room.Room
import com.android.base.data.database.*
import com.android.base.utils.obj.Constants.DATABASE_NAME_MESSAGE
import com.android.base.utils.obj.Constants.DATABASE_NAME_STATIC
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Singleton
    @Provides
    fun provideMessageDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        MessageDatabase::class.java,
        DATABASE_NAME_MESSAGE
    ).fallbackToDestructiveMigration().build()

    @Singleton
    @Provides
    fun provideMessageDao(db: MessageDatabase) = db.getMessageDao()

    @Singleton
    @Provides
    fun provideMessageRepository(dao: MessageDAO): MessageRepository = MessageRepositoryImpl(dao)


    /** chart **/

    @Provides
    fun provideStaticDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        StaticDatabase::class.java,
        DATABASE_NAME_STATIC
    ).fallbackToDestructiveMigration().build()

    @Singleton
    @Provides
    fun provideStaticDao(db: StaticDatabase) = db.getStaticDao()

    @Singleton
    @Provides
    fun provideStaticRepository(dao: StaticDao): StaticRepository = StaticRepositoryImpl(dao)
}
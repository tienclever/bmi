package com.android.base.utils.extension

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

const val FORMAT_FULL = "EEE MMM dd HH:mm:ss Z yyyy"
const val FORMAT_DATE_TIME = "dd/MM/yy"

fun String.formatToString(): String {
    val inFormat = SimpleDateFormat(FORMAT_FULL)
    val outFormat = SimpleDateFormat(FORMAT_DATE_TIME)

    var date: Date? = null
    try {
        date = inFormat.parse(this)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return date?.let { outFormat.format(it) } ?: String()
}

fun String.formatDate(): Date? {
    val format = SimpleDateFormat(FORMAT_FULL)
    return try {
        val date = format.parse(this)
        date
    } catch (e: ParseException) {
        null
    }
}

fun Date.formatString(): String {
    val dateFormat = SimpleDateFormat(FORMAT_FULL)
    return try {
        val dateTime = dateFormat.format(this)
        dateTime ?: String()
    } catch (e: ParseException) {
        String()
    }
}



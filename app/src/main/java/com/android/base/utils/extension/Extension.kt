package com.android.base.utils.extension

import android.app.ActionBar
import android.app.Activity
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy


fun View.enableView(isEnable: Boolean) {
    this.isEnabled = isEnable
    this.isClickable = isEnable
    alpha = if (isEnable) 1f else 0.7f
}

fun ImageView.loadImage(url: String?, placeholder: Drawable?) {
    Glide.with(this)
        .load(url)
        .error(placeholder)
        .placeholder(placeholder)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(this)
}

fun getResult(BMI: Float): String {
    val result = when (BMI) {
        in 0f..16f -> "Severe thinness"
        in 16f..17f -> "Moderate thinness"
        in 17f..18.5f -> "Mild thinness"
        in 18.5f..25f -> "Normal"
        in 25f..30f -> "Overweight"
        in 30f..35f -> "Obese Class I"
        in 35f..40f -> "Obese Class II"
        else -> "Obese Class III"
    }

    return result
}

fun calculateBmi(weight: Float, height: Float): String {
    val bmi = weight / ((height / 100) * (height / 100))
    return getResult(bmi)
}

fun Activity.hideStatus() {
    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
    actionBar?.hide()
}
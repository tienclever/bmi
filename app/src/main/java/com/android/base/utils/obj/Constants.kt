package com.android.base.utils.obj

object Constants {
    const val DEFAULT_TIMEOUT = 30L

    const val DATABASE_NAME_MESSAGE = "DATABASE_NAME_MESSAGE"
    const val DATABASE_NAME_STATIC = "DATABASE_NAME_STATIC"
    const val DATA_STORE_NAME = "AppPref"
}

object Key {
    const val STATISTIC = "STATISTIC"
}
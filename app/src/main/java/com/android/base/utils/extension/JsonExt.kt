package com.android.base.utils.extension

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

fun <T> String.safeFromJson(modelClass: Class<T>): T? {
    return try {
        Gson().fromJson(this, modelClass)
    } catch (e: Exception) {
        null
    }
}

fun Any.safeToJson(): String {
    return try {
        return Gson().toJson(this)
    } catch (e: Exception) {
        String()
    }
}

fun <T> Gson.fromJsonList(jsonString: String): List<T> =
    this.fromJson(jsonString, object : TypeToken<ArrayList<T>>() {}.type)
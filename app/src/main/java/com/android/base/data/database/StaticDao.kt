package com.android.base.data.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.android.base.data.database.entity.Statistic

@Dao
interface StaticDao {
    @Query("SELECT * FROM statistic")
    fun getAll(): List<Statistic>

    @Insert
    fun insertAll(vararg statistic: Statistic)

    @Delete
    fun delete(statistic: Statistic)
}
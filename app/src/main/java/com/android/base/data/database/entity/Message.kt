package com.android.base.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Message")
data class Message(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var content: String = "",
)
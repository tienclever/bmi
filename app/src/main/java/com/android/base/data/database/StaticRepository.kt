package com.android.base.data.database

import com.android.base.data.database.entity.Statistic
import kotlinx.coroutines.flow.Flow

interface StaticRepository {

    fun getAllStatistic(): Flow<List<Statistic>>

    fun insertStatistic(statistic: Statistic): Flow<Unit>

    fun deleteStatistic(statistic: Statistic): Flow<Unit>
}
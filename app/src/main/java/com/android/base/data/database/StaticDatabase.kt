package com.android.base.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.base.data.database.entity.Statistic

@Database(entities = [Statistic::class], version = 1)
abstract class StaticDatabase : RoomDatabase() {
    abstract fun getStaticDao(): StaticDao
}
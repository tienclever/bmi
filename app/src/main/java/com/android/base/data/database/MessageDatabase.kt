package com.android.base.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.base.data.database.entity.Message

@Database(
    entities = [Message::class],
    version = 1
)
abstract class MessageDatabase : RoomDatabase() {
    abstract fun getMessageDao(): MessageDAO
}
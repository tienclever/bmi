package com.android.base.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Statistic(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    @ColumnInfo(name = "date_time")
    var dateTime: String? = null,
    @ColumnInfo(name = "age")
    val age: String? = null,
    @ColumnInfo(name = "weight")
    val weight: String? = null,
    @ColumnInfo(name = "height")
    val height: String? = null,
    @ColumnInfo(name = "bmi")
    val bmi: String? = null
) : Serializable {

    override fun toString(): String {
        return "$dateTime.$age.$weight.$height.$bmi"
    }
}


package com.android.base.data.remote

import com.android.base.data.remote.dto.CoinDetail
import com.android.base.data.remote.dto.Coin
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    @GET("/v1/coins")
    suspend fun getCoins(): List<Coin>

    @GET("/v1/coins/{coinId}")
    suspend fun getCoinById(@Path("coinId") coinId: String): CoinDetail
}
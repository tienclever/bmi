package com.android.base.data.database

import com.android.base.data.database.entity.Statistic
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class StaticRepositoryImpl @Inject constructor(
    private val staticDao: StaticDao
) : StaticRepository {

    override fun getAllStatistic(): Flow<List<Statistic>> {
        return flow {
            emit(staticDao.getAll())
        }
    }

    override fun insertStatistic(statistic: Statistic): Flow<Unit> {
        return flow {
            staticDao.insertAll(statistic)
            emit(Unit)
        }
    }

    override fun deleteStatistic(statistic: Statistic): Flow<Unit> {
        return flow {
            staticDao.delete(statistic)
            emit(Unit)
        }
    }
}
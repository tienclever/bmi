package com.android.base.presentation.home

import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import com.android.base.R
import com.android.base.data.database.entity.Statistic
import com.android.base.databinding.FragmentHomeBinding
import com.android.base.navigation.AppNavigation
import com.android.base.presentation.base.BaseFragment
import com.android.base.utils.extension.calculateBmi
import com.android.base.utils.extension.formatString
import com.android.base.utils.obj.Key
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(R.layout.fragment_home) {

    @Inject
    lateinit var appNavigation: AppNavigation

    private val viewModel: HomeViewModel by viewModels()
    override fun getVM(): HomeViewModel = viewModel

    override fun setListeners() {
        super.setListeners()

        binding.calculateBtn.setOnClickListener {
            handleCalculator()
        }
    }

    private fun handleCalculator() {
        val weight = binding.weightEt.text.toString().toFloatOrNull() ?: return
        val height = binding.heightEt.text.toString().toFloatOrNull() ?: return
        val age = binding.ageEt.text.toString().toIntOrNull() ?: return

        val statistic = Statistic(
            age = age.toString(),
            bmi = calculateBmi(weight, height),
            weight = weight.toString(),
            height = height.toString()
        )

        openDetail(statistic)
    }

    private fun openDetail(statistic: Statistic) {
        appNavigation.openTopToDetail(bundleOf(Key.STATISTIC to statistic))
    }
}
package com.android.base.presentation.setting

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.android.base.R
import com.android.base.databinding.FragmentSettingBinding
import com.android.base.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingFragment : BaseFragment<FragmentSettingBinding, SettingViewModel>(R.layout.fragment_setting) {

    private val viewModel: SettingViewModel by viewModels()
    override fun getVM(): SettingViewModel = viewModel

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
    }
}
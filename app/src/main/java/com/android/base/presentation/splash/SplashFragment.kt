package com.android.base.presentation.splash

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.android.base.R
import com.android.base.databinding.FragmentSplashBinding
import com.android.base.navigation.AppNavigation
import com.android.base.presentation.base.BaseFragmentNotRequireViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment :
    BaseFragmentNotRequireViewModel<FragmentSplashBinding>(R.layout.fragment_splash) {

    @Inject
    lateinit var appNavigation: AppNavigation

    override fun initView(savedInstanceState: Bundle?) {
        lifecycleScope.launch {
            delay(1000)
            openSplashToHome()
        }
    }

    private fun openSplashToHome() {
        appNavigation.openSplashToTop()
    }
}
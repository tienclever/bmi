package com.android.base.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.android.base.data.database.StaticRepository
import com.android.base.data.database.entity.Statistic
import com.android.base.presentation.base.BaseViewModel
import com.android.base.utils.extension.formatString
import com.android.base.utils.obj.Key
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val staticRepository: StaticRepository,
    savedStateHandle: SavedStateHandle
) : BaseViewModel() {
    private val _statistics = MutableLiveData<List<Statistic>>()
    val statistics: LiveData<List<Statistic>> = _statistics

    private val statistic = savedStateHandle.get<Statistic>(Key.STATISTIC)

    init {
        getAllStatic()
    }

    fun saveStatic() {
        statistic ?: return

        statistic.dateTime = Calendar.getInstance().time.formatString()

        viewModelScope.launch {
            staticRepository
                .insertStatistic(statistic)
                .flowOn(Dispatchers.IO)
                .catch { ex -> ex.printStackTrace() }
                .collect { getAllStatic() }
        }
    }

    private fun getAllStatic() {
        viewModelScope.launch {
            staticRepository
                .getAllStatistic()
                .flowOn(Dispatchers.IO)
                .catch { ex -> ex.printStackTrace() }
                .collect { statics ->
                    withContext(Dispatchers.Main) {
                        _statistics.value = statics
                    }
                }
        }
    }
}
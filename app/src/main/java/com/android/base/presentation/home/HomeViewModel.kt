package com.android.base.presentation.home

import androidx.lifecycle.viewModelScope
import com.android.base.data.remote.dto.Coin
import com.android.base.data.repository.CoinRepository
import com.android.base.presentation.base.BaseViewModel
import com.android.base.utils.model.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

private const val TAG = "HomeViewModel"

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val coinRepository: CoinRepository
) : BaseViewModel() {
    private val _coinList: MutableStateFlow<List<Coin>> = MutableStateFlow(emptyList())
    val coinList: MutableStateFlow<List<Coin>> = _coinList

    private fun getCoins() {
        coinRepository.getCoins()
            .onCompletion { _isLoading.value = false }
            .onEach { result -> handleResult(result) }
            .launchIn(viewModelScope)
    }

    private fun handleResult(result: Resource<List<Coin>>) {
        when (result) {
            is Resource.Success -> {
                _coinList.value = result.data ?: emptyList()
            }
            is Resource.Error -> {
                messageError.value = result.message
            }
            is Resource.Loading -> {
                _isLoading.value = true
            }
        }
    }
}
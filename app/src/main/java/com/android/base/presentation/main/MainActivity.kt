package com.android.base.presentation.main

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import com.android.base.R
import com.android.base.databinding.ActivityMainBinding
import com.android.base.navigation.AppNavigation
import com.android.base.presentation.base.BaseActivity
import com.android.base.utils.extension.hideStatus
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    @Inject
    lateinit var appNavigation: AppNavigation

    override val layoutId: Int = R.layout.activity_main
    override fun getVM(): MainViewModel = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        hideStatus()

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host) as NavHostFragment
        appNavigation.bind(navHostFragment.navController)
    }
}
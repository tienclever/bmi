package com.android.base.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.android.base.data.remote.dto.Coin
import com.android.base.databinding.ItemCoinBinding

class CoinsAdapter constructor(
    private val context: Context,
    private val onClickItem: (Coin) -> Unit
) : ListAdapter<Coin, CoinsAdapter.CoinViewHolder>(CoinDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinViewHolder {
        return CoinViewHolder(
            ItemCoinBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), onClickItem
        )
    }

    override fun onBindViewHolder(holder: CoinViewHolder, position: Int) {
        holder.bindData(currentList[position])
    }

    class CoinViewHolder(
        private val binding: ItemCoinBinding,
        private val onClickItem: (Coin) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(coin: Coin) {
            binding.apply {
                tvName.text = "${coin.rank}. ${coin.name} (${coin.symbol})"
                tvStatus.text = if (coin.isActive) "active" else "inactive"

                root.setOnClickListener {
                    onClickItem.invoke(coin)
                }
            }
        }
    }
}

class CoinDiffUtil : DiffUtil.ItemCallback<Coin>() {
    override fun areItemsTheSame(oldItem: Coin, newItem: Coin): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Coin, newItem: Coin): Boolean {
        return oldItem == newItem
    }

}

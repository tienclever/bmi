package com.android.base.presentation.top

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI.setupWithNavController
import com.android.base.R
import com.android.base.databinding.FragmentTopBinding
import com.android.base.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TopFragment : BaseFragment<FragmentTopBinding, TopViewModel>(R.layout.fragment_top) {

    private val viewModel: TopViewModel by viewModels()
    override fun getVM(): TopViewModel = viewModel

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        setUpNav()
    }

    private fun setUpNav() {
        (childFragmentManager.findFragmentById(R.id.container) as? NavHostFragment)?.let {
            setupWithNavController(binding.bottomNavigation, it.navController)
        }
    }
}
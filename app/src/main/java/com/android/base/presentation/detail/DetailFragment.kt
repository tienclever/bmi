package com.android.base.presentation.detail

import android.os.Bundle
import androidx.fragment.app.viewModels
import com.android.base.R
import com.android.base.databinding.FragmentDetailBinding
import com.android.base.navigation.AppNavigation
import com.android.base.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailFragment : BaseFragment<FragmentDetailBinding, DetailViewModel>(R.layout.fragment_detail) {
    @Inject
    lateinit var appNavigation: AppNavigation

    private val viewModel: DetailViewModel by viewModels()
    override fun getVM(): DetailViewModel = viewModel

    private val mAdapter by lazy {
        DetailAdapter(requireContext())
    }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        setUpToolbar()
        setUpTopLayout()
        setUpBottomLayout()
    }

    override fun setListeners() {
        super.setListeners()

        binding.ivSave.setOnClickListener {
            viewModel.saveStatic()
        }
    }

    private fun setUpToolbar() {
        binding.toolbar.apply {
            ivBack.setOnClickListener {
                appNavigation.navigateUp()
            }
        }
    }

    private fun setUpTopLayout() {
        binding.layoutTop.apply {}
    }

    private fun setUpBottomLayout() {
        binding.layoutBottom.apply {
            rcvDetail.apply {
                itemAnimator = null
                setHasFixedSize(true)
                adapter = mAdapter
            }
        }
    }

    override fun observeData() {
        super.observeData()

        viewModel.statistics.observe(viewLifecycleOwner) { statics ->
            mAdapter.submitList(statics) {
                if (statics.isNotEmpty()) {
                    val endPos = statics.size - 1
                    binding.layoutBottom.rcvDetail.smoothScrollToPosition(endPos)
                }
            }
        }
    }
}
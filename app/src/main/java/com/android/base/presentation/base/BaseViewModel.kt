package com.android.base.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.base.utils.event.SingleLiveEvent

abstract class BaseViewModel : ViewModel() {
    var _isLoading = MutableLiveData(false)

    var isLoading: LiveData<Boolean> = _isLoading
    var messageError = SingleLiveEvent<Any>()
}

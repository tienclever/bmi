package com.android.base.presentation.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.android.base.data.database.entity.Statistic
import com.android.base.databinding.ItemStaticBinding

class DetailAdapter(context: Context) : ListAdapter<Statistic, ItemViewHolder>(DetailAdapterDiffUtil()) {
    private val layoutInflater by lazy {
        LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(ItemStaticBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

class ItemViewHolder(private val binding: ItemStaticBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(statistic: Statistic) {
        binding.statistic = statistic
        binding.executePendingBindings()
    }
}

class DetailAdapterDiffUtil : DiffUtil.ItemCallback<Statistic>() {
    override fun areItemsTheSame(oldItem: Statistic, newItem: Statistic): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Statistic, newItem: Statistic): Boolean {
        return oldItem == newItem
    }
}
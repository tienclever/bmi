package com.android.base.presentation.history

import androidx.fragment.app.viewModels
import com.android.base.R
import com.android.base.databinding.FragmentHistoryBinding
import com.android.base.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HistoryFragment: BaseFragment<FragmentHistoryBinding, HistoryViewModel>(R.layout.fragment_history) {

    private val viewModel: HistoryViewModel by viewModels()
    override fun getVM(): HistoryViewModel = viewModel
}